const User = require('../src/model/user')
const Task =require('../src/model/task')
require('../src/db/mongoose')
const userUpdate=async(id,age)=>{
   await User.findByIdAndUpdate(id,{age})
   const count=await User.countDocuments({age})
   return count
}

userUpdate('5ead8ad339848615c0b73922',4).then((result)=>{
    console.log(result)
}).catch((e)=>{
   console.log(e)
})
// User.findByIdAndUpdate('5ead8ec20b9b3a1a641b1e1f',{age:1}).then((user)=>{
//     return User.countDocuments({age:1})
// }).then((count)=>{
//     console.log(count)
// }).catch((e)=>{
//     console.log(e)
// })
