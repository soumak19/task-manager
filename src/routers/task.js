const express=require('express')
const router =new express.Router()
const Task=require('../model/task')
const auth =require('../middleware/auth')
router.post('/tasks',auth,async(req,res)=>{
   // const task=new Task(req.body)
   const task=new Task({
       ...req.body,
       owner:req.user._id
   })
    try{
      const data= await task.save()
       res.status(201).send(data)
    }catch(e)
    {
        res.status(400).send(e)
    }
})

//GET/tasks?completed=true
//GET/tasks?limit=10&skip=20
//GET/tasks?sortBy=createdAt:desc
router.get('/tasks',auth,async(req,res)=>{
    console.log(req.query)
      const match={}
      const sort={}
     
      
      if(req.query.completed){
          match.completed=req.query.completed ==='true'
      }
      if(req.query.sortBy){
        const parts=req.query.sortBy.split(':')
        sort[parts[0]]=parts[1]==='desc'?-1:1
      }
      
    try{
      const user=  await req.user.populate({
          path:'tasks',
          match,
          options:{
              limit:parseInt(req.query.limit),
              skip:parseInt(req.query.skip),
              sort
          }
      }).execPopulate()
        if(!user.tasks){
            return res.status(404).send()
         }
         res.send(user.tasks)
    }catch(e){
        res.status(500).send()
    }
})


// router.get('/tasks/:id',async(req,res)=>{
//     const _id=req.params.id
//     try{
//        const taskbyId= Task.findById(_id) 
//        if(!taskbyId){
//                return res.status(404).send()
//             }
//      res.send(taskbyId)

//     }catch(e){
//         res.status(500).send()
       
//     }
// })


router.get('/tasks/:id',auth,async(req,res)=>{
    try{
       const task=await Task.findOne({_id:req.params.id,owner:req.user._id})
       if(!task){
               return res.status(404).send()
     }
     res.send(task)

    }catch(e){
        res.status(500).send()
       
    }
})



// router.patch('/tasks/:id',async(req,res)=>{
//     const allowedTask= ['descripton','completed']
//     const upadteTask=Object.keys(req.body)
//     const isValidOperation=upadteTask.every((task)=>allowedTask.includes(task))
//     if(!isValidOperation){
//         res.status(400).send({Error:'Inavlid update tasks!!'})
//     }
//     try{
//      //  const updatedTask= await Task.findByIdAndUpdate(req.params.id,req.body,{new:true,runValidators:true})
//      const task= await Task.findById(req.params.id)
     
//      upadteTask.forEach((Task)=>task[Task]=req.body[Task])
     
//      task.save()
     
//        if(!task){
//            res.status(404).send();
//        }
//        res.send(task)
//     }catch(e){
//        res.status(500).send(e)
//     }
// })

router.patch('/tasks/:id',auth,async(req,res)=>{
    const allowedTask= ['descripton','completed']
    const upadteTask=Object.keys(req.body)
    const isValidOperation=upadteTask.every((task)=>allowedTask.includes(task))
    if(!isValidOperation){
        res.status(400).send({Error:'Inavlid update tasks!!'})
    }
    try{
     //  const updatedTask= await Task.findByIdAndUpdate(req.params.id,req.body,{new:true,runValidators:true})
     const task= await Task.findOne({_id:req.params.id,owner:req.user._id})
     if(!task){
        res.status(404).send();
    }
     upadteTask.forEach((Task)=>task[Task]=req.body[Task])
     task.save()
       
       res.send(task)
    }catch(e){
       res.status(500).send(e)
    }
})

router.delete('/tasks/:id',auth,async(req,res)=>{
  try{
      const task=await Task.findOneAndDelete({_id:req.params.id,owner:req.user._id})
      if(!task){
          res.status(404).send()
      }
      res.send(task)

  }catch(e){
     res.status(500).send()
  }
})

module.exports= router