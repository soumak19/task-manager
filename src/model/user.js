const mongoose=require('mongoose')
const validator=require('validator')
const bcrypt=require('bcryptjs')
const jwt=require('jsonwebtoken')
const Task=require('./task')


const userSchema=new mongoose.Schema({
    name:{
        type:String,
        required:true,
        trim:true
    },
    email:{
        type:String,
        required:true,
        unique:true,
        trim:true,
        lowercase:true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Email is invalid!!')
            }
        }

    },
    age:{
         type:Number,
         default:0,
         validate(value){
             if(value<0)
             throw new Error('Age must be a positive number!!')
         }
    },
    password:{
        type:String,
        required:true,
        trim:true,
        minlength:6,
        validate(value){
            if(value.toLowerCase().includes("password")){
              throw new Error('Password should not contain "password"')
            }
        }


    },
    tokens:[{
        token: {
            type:String,
            require:true
   
       }
    }
       ],
       avatar:{
           type:Buffer
       }

},{
    timestamps:true
})

userSchema.virtual('tasks',{
    ref:'Task',
    localField:'_id',
    foreignField:'owner'
})

userSchema.statics.findByCredentials=async(email,password)=>{
    const user= await User.findOne({email})
    if(!user){
        throw new Error('Invalid user!!')
    }
    //console.log(password+" "+user.password)
    const isMatch=await bcrypt.compare(password,user.password)
    
    if(!isMatch){
      throw new Error('Invalid user!!')
    }
  
    return user

}

userSchema.methods.generateAuthToken=async function(){
    const user=this
    const token=jwt.sign({_id:user._id.toString()},process.env.jwt_SECRET)
    user.tokens=user.tokens.concat({token})
    await user.save()
    return token
 }

 userSchema.methods.toJSON=function(){
    const user=this
    const userObject=user.toObject()
    delete userObject.password
    delete userObject.tokens
    delete userObject.avatar
    return userObject
   
 }
//Has the plan text password before saving
userSchema.pre('save', async function(next){
   const user=this
   if(user.isModified('password')){
      user.password = await bcrypt.hash(user.password,8)
   }
   next()
})

//delete user task when user is removed
userSchema.pre('remove', async function(next){
    const user=this
   await Task.deleteMany({owner:user._id})
    
    next()
 })



const User=mongoose.model('User',userSchema)
// const me =new User({
//     name:'Monica',
//     email:' MONICA@GMAIL.com',
//     age:24,
//     password:'password123'
// })
// me.save().then((result)=>{
//   console.log(result)
// }).catch((error)=>{
//   console.log(error)
// })
module.exports=User