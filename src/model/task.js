const mongoose=require('mongoose')
const validator = require('validator')
const taskSchema=new mongoose.Schema(
    {
        descripton:{
            type:String,
            required:true
        },
        completed:{
            type:Boolean,
            default:false
        },
        owner:{
             type:mongoose.Schema.Types.ObjectId,
             required:true,
             ref:'User'
             
        }
     },{
         timestamps:true
     }
)
const Task=mongoose.model('Task',taskSchema)

module.exports=Task

// const someTask= new Task({
//     descripton:'take a bath',
    
// })
// someTask.save().then(()=>{
//     console.log(someTask)
// }).catch(()=>{
//     console.log('Error!',error)
// })