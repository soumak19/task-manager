const request=require('supertest')
const app=require('../src/app')
const User=require('../src/model/user')
const {  userId, userOne,setUpDataBase}=require('./fixtures/db')


beforeEach(setUpDataBase)

test('should signup a new user',async()=>{
  const response = await request(app).post('/users').send({
       name:'ross',
       email:'ross@example.com',
       password:'pass!@#12'
   }).expect(201)

   //Assert that the database was changed correctlly
   const user = await User.findById(response.body.user._id)
   expect(user).not.toBeNull()
   //Assertion about the response
   expect(response.body).toMatchObject({
       user:{
           name:'ross',
           email:'ross@example.com'
       },
       token:user.tokens[0].token
   })
  expect(user.password).not.toBe('pass!@#12')


})

test('should login a new user',async()=>{
    const response =  await request(app).post('/users/login').send({
        email:userOne.email,
        password:userOne.password
    }).expect(200)
    
   
     const user = await User.findById(userId)
    expect(response.body.token).toBe(user.tokens[1].token)
 })

 test('should not login nonexistent user',async()=>{
    await request(app).post('/users/login').send({
        email:userOne.email,
        password:'thisismywrongpass'
    }).expect(400)
 })
 
 test('should get profile for user',async()=>{
    await request(app).
    get('/users/me')
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send()
    .expect(200)
 })


 test('should not get profile for unautherization user',async()=>{
    await request(app).
     get('/users/me')
    .send()
    .expect(401)
 })

 test('should delete  account  for  Authenticated user',async()=>{
    await request(app).
    delete('/users/me')
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .send()
    .expect(200)

    const user=await User.findById(userId)
    expect(user).toBeNull()
 })

 test('should not delete  account  for  Unauthenticated user',async()=>{
    await request(app).
    delete('/users/me')
    .send()
    .expect(401)
 })

 test('Should upload avatar image',async()=>{
     await request(app)
     .post('/users/me/avatar')
     .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
     .attach('avatar','tests/fixtures/profile-pic.jpg')
     .expect(200)

     const user = await User.findById(userId)
     expect(user.avatar).toEqual(expect.any(Buffer))
 })

 test('Should upadate valid user field',async()=>{
    await request(app)
    .patch('/users/me').send({
           name:'mon',
           email:'mon@example.com'
    })
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .expect(200)

    const user=await User.findById(userId)
    expect(user.name).toEqual('mon')
})

test('Should not upadate valid user field',async()=>{
    await request(app)
    .patch('/users/me').send({
          location:'MUMBAI'
    })
    .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
    .expect(400)

})
