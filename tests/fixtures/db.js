const mongoose=require('mongoose')
const jwt=require('jsonwebtoken')
const User=require('../../src/model/user')
const userId= new mongoose.Types.ObjectId
const userOne={
    _id:userId,
    name:'Mike',
    email:'mike@example.com',
    password:'22assd@21',
    tokens:[{
        token:jwt.sign({_id:userId},process.env.jwt_SECRET)
    }]


}
const setUpDataBase = async()=>{
    await User.deleteMany()
    await new User(userOne).save()
}

module.exports = {
    userId,
    userOne,
    setUpDataBase
}